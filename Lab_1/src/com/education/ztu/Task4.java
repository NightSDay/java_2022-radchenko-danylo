package com.education.ztu;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("num 1 = ");
        int num1 = sc.nextInt();

        System.out.print("num 2 = ");
        int num2 = sc.nextInt();

        int divider = 0;

        for (int i = 1; i <= num1 && i <= num2; i++) {
            if (num1 % i == 0 && num2 % i == 0) {
                divider = i;
            }
        }

        System.out.println("Найбільший спільний дільник: " + divider);

    }
}
