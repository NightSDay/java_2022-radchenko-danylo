package com.education.ztu;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("n = ");
        int n = scanner.nextInt();

        int[] fibonacci = new int[n];

        if (n >= 2) {
            fibonacci[0] = 1;
            fibonacci[1] = 1;
        } else if (n == 1) {
            fibonacci[0] = 1;
        }

        for (int i = 2; i < n; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

        printArray(fibonacci);

        int[] reverseFibonacci = new int[n];
        for (int i = n - 1; i >= 0; i--) {
            reverseFibonacci[n - i - 1] = fibonacci[i];
        }

        printArray(reverseFibonacci);
    }

    public static void printArray(int[] arr) {
        System.out.print("[");

        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + ", ");
        }

        if (arr.length > 0) {
            System.out.print(arr[arr.length - 1]);
        }

        System.out.println("]");
    }
}