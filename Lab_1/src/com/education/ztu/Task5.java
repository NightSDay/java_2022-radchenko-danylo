package com.education.ztu;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введіть значення: ");
        int number = sc.nextInt();
        System.out.print("Sum of digits:  "+ getDigitSum(number));

        sc.close();
    }
    public static int getDigitSum(int number){
        String str = String.valueOf(number);
        String[] array = str.split("");
        int result = 0;
        for(int i=0;i<array.length;i++){
            result += Integer.parseInt(array[i]);
        }
        return result;
     }
}
