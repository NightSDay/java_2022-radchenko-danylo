import exceptions.WrongLoginException;
import exceptions.WrongPasswordException;

public class Task2 {
    public static void main(String[] args) {
        System.out.println(checkCredentials("danylo_radchenko_2003", "password", "password"));
        System.out.println(checkCredentials("danyloradchenko25062003", "password", "password"));
        System.out.println(checkCredentials("данило_радченко", "password", "password"));
        System.out.println(checkCredentials("danylo_radchenko", "password", "password"));
    }

    public static boolean checkCredentials(String login, String password, String confirmPassword) {

        try {
            if (!login.matches("([A-Za-z0-9_])+"))
                throw new WrongLoginException("Login must be only from latin letters, numbers and underlines");
            if (login.length() > 20)
                throw new WrongLoginException("Login must be less than 20 symbols length.");

            if (!password.matches("([A-Za-z0-9_])+"))
                throw new WrongPasswordException("Password must be only from latin letters, numbers and underlines");
            if (password.length() > 20)
                throw new WrongPasswordException("Password must be less than 20 symbols length.");
            if (!password.equals(confirmPassword))
                throw new WrongPasswordException("Passwords must be the same.");

            return true;
        }
        catch (WrongLoginException | WrongPasswordException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
