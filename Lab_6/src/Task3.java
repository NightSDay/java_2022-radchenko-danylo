import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Task3 {
    public static final String FILES_PATH = "directory_for_files/";
    public static final String FILE_NAME = "task3.txt";

    public static void main(String[] args) {
        Object[][] products = new Object[][]{
                {"Кофта", "Жіночий одяг", 1510.78},
                {"Туфлі", "Жіночий одяг", 1400.56},
                {"Рубашка", "Чоловічий одяг", 800.78},
                {"Капці", "Дитячий одяг", 119.99},
                {"Юбка", "Жіночий одяг", 149.50},
                {"Светр", "Дитячий одяг", 329.89},
                {"Фрак", "Чоловічий одяг", 599.90}
        };

        List<String> bill = getBill(products);
        writeListToFile(bill);
        readFileToConsole(new File(FILES_PATH, FILE_NAME));
    }

    private static List<String> getBill(Object[][] products) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        String border = "=".repeat(42);
        float sum = 0.00f;

        List<String> billLines = new ArrayList<>();

        billLines.add(String.format("%-21s%21s%n", "Дата та час покупки", dtf.format(localDateTime)));
        billLines.add(String.format("%s%n", border));
        billLines.add(String.format("%-4s%-11s%-18s%-9s%n", "№", "Товар", "Категорія", "Ціна"));
        billLines.add(String.format("%s%n", border));

        for (int item = 0; item < products.length; item++) {
            billLines.add(String.format("%-4s%-11s%-18s%-9s%n", item + ".", products[item][0], products[item][1], products[item][2] + " ₴"));
            sum += Float.parseFloat(products[item][2].toString());
        }

        billLines.add(String.format("%s%n", border));
        billLines.add(String.format("%-21s%21s%n", "Разом:", sum + " ₴"));

        return billLines;
    }

    public static void writeListToFile(List<String> list) {
        File dir = new File(FILES_PATH);
        if (!dir.exists())
            if (dir.mkdir())
                System.out.println("Directory is created successfully");

        try {
            FileWriter writer = new FileWriter(new File(FILES_PATH, FILE_NAME));
            for (String line : list) {
                writer.write(line);
            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void readFileToConsole(File file) {
        try {
            FileReader reader = new FileReader(file);
            int i;
            while ((i = reader.read()) != -1) {
                System.out.print((char) i);
            }
            reader.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
