import java.io.File;
import java.io.RandomAccessFile;

public class Task5 {
    public static final String FILES_PATH = "directory_for_files/";
    public static final String FILE_NAME = "task3.txt";

    public static void main(String[] args) {
        File file = new File(FILES_PATH, FILE_NAME);
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            raf.seek(200);
            raf.write("###TEST###".getBytes());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
