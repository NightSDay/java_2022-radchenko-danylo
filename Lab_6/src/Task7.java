import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Task7 {
    public static final String FILES_PATH = "directory_for_files/";
    public static final String ZIP_FILE_NAME = "zipFile.zip";

    public static void main(String[] args) {
        File zipFile = zipDirectory(FILES_PATH);
        System.out.println();
        printZipFile(zipFile);
    }

    public static File zipDirectory(String directory) {// створив метод зіп діректорі
        File dir = new File(directory);//в яку передаю папку яку треба було зархівувати
        File[] innerElements = dir.listFiles();//отримую з неї всі файли і перевіряю чи вона не пуста

        if (innerElements == null) {
            System.out.println("Directory is empty!");
            return null;
        }

        File newZipFile = new File(ZIP_FILE_NAME);//через файлаутстрім створюю зіпфайл
        try (FileOutputStream fileOutputStream = new FileOutputStream(newZipFile)) {//відкриваю потік для запису
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);//

            for (File elem : innerElements) {//проходжусь по всім файлам з масиву якій отримав
                if (elem.isDirectory()) {//і зробив два методи які перевіряють,якщо це дерикторія то викликається зіпфолдер
                    zipFolder(elem, elem.getName() + "/", zipOutputStream);
                } else {//а якщо файл то зіпфайл
                    zipFile(elem, "", zipOutputStream);
                }
            }

            zipOutputStream.finish();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return newZipFile;
    }
    //якщо це директорія то за допомогою рекурсії,ще раз отримую файли з директорії,якщо пусто то всерівно спрацює
    public static void zipFolder(File folder, String parentFolder, ZipOutputStream zipOutputStream) throws IOException {
        System.out.format("Folder [%s] is added\n", folder.getName());
        File[] files = folder.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                String folderPath = parentFolder + file.getName() + "/";
                zipFolder(file, folderPath, zipOutputStream);
            } else {
                zipFile(file, parentFolder, zipOutputStream);
            }
        }
    }
    //якщо це файл то він зберігаєтсяь за допомогою  зіпентрі і за допомогою пут некст я його вкладаю
    public static void zipFile(File file, String parentFolder, ZipOutputStream zipOutputStream) throws IOException {
        ZipEntry zip = new ZipEntry(parentFolder + file.getName());
        zipOutputStream.putNextEntry(zip);
        System.out.format("File [%s] is added\n", parentFolder + file.getName());
    }

    public static void printZipFile(File zip) {
        try (FileInputStream fileInputStream = new FileInputStream(zip)) {
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);

            while(zipInputStream.available() == 1) {
                ZipEntry entry = zipInputStream.getNextEntry();
                if (entry != null)
                    System.out.println(entry.getName());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

