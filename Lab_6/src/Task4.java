import java.io.*;

public class Task4 {
    public static final String FILES_PATH = "directory_for_files/";
    public static final String FILE_NAME = "task3.txt";
    public static final String IMAGES_PATH = "files/";
    public static final String IMAGE_NAME = "valkirya.jpg";

    public static void main(String[] args) {
        copyFile(FILES_PATH, new File(FILES_PATH, FILE_NAME));
        copyImage(FILES_PATH, new File(IMAGES_PATH, IMAGE_NAME));
    }

    public static void copyFile(String copyToPath, File file) {
        File newFile = new File(copyToPath, "copy_" + file.getName());
        try (FileReader reader = new FileReader(file);
             FileWriter writer = new FileWriter(newFile)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void copyImage(String copyToPath, File file) {
        File newFile = new File(copyToPath, "copy_" + file.getName());
        try (InputStream inputStream = new FileInputStream(file);
             OutputStream outputStream = new FileOutputStream(newFile)){
            int i;
            while ((i = inputStream.read()) != -1) {
                outputStream.write(i);
            }
            outputStream.flush();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
