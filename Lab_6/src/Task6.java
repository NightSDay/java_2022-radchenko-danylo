import java.io.File;

public class Task6 {
    public static final String FILES_PATH = "directory_for_files/";
    public static final String NEW_DIR = "inner_directory/";

    public static void main(String[] args) {
        //Створити нову папку з ім'ям inner_directory
        File dir = new File(FILES_PATH + NEW_DIR);

        boolean D1 = dir.mkdir();
        if (D1) {
            System.out.println("Directory is created successfully");
        } else {
            System.out.println("Directory is already exist");
        }

        // Вивести абсолютний шлях створеної папки
        System.out.println("Absolute path: " + dir.getAbsolutePath());

        // Вивести ім’я батьківської директорії
        System.out.println("Parent: " + dir.getParent());

        // Створити два текстових файли всередині папки inner_directory.
        File file1 = new File(FILES_PATH + NEW_DIR, "file1");
        File file2 = new File(FILES_PATH + NEW_DIR, "file2");
        try {
            if (file1.createNewFile())
                System.out.println("File1 created");
            else
                System.out.println("File1 already exists");

            if (file2.createNewFile())
                System.out.println("File2 created");
            else
                System.out.println("File2 already exists");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Один файл видалити
        if (file1.delete())
            System.out.println("File1 deleted");
        else
            System.out.println("File1 not deleted");

        // Переіменувати папку inner_directory в renamed_inner_directory
        if (dir.renameTo(new File(FILES_PATH + "renamed_inner_directory"))) {
            System.out.println("Directory renamed successfully");
        } else {
            System.out.println("Failed to rename directory");
        }

        // Вивести список файлів та папок в папці directory_for_files, їх розмір та тип (файл, папка)
        File parentDir = dir.getParentFile();
        File[] innerElements = parentDir.listFiles();

        if (innerElements != null) {
            for (File elem : innerElements) {
                if(elem.isFile()){
                    System.out.println("File [" + elem.getName() + "]; size: [" + elem.getTotalSpace() + "]");
                }else {
                    System.out.println("Directory [" + elem.getName() + "]; size: [" + elem.getTotalSpace() + "]");
                }
            }
        }
    }
}
