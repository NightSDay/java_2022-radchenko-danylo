package exceptions;

public class WrongLoginException extends Exception{

    public WrongLoginException() {
        super("WrongLoginException: login should contain only latin letters, number and underline");
    }
    public WrongLoginException(String reason) {
        super("WrongLoginException: " + reason);
    }
}
