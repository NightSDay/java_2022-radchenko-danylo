package exceptions;

public class WrongPasswordException extends Exception {

    public WrongPasswordException() {
        super("WrongPasswordException: password should contain only latin letters, number, underline, and matches with confirm password");
    }
    public WrongPasswordException(String reason) {
        super("WrongPasswordException: " + reason);
    }
}
