package com.education.ztu;

import com.education.ztu.game.Employee;
import com.education.ztu.game.LeagueTeam;
import com.education.ztu.game.Schoolar;
import com.education.ztu.game.Student;

public class Main {

    public static void main(String[] args) {
        try {
            // Schoolar
            title("Schoolar");
            Schoolar schoolar = new Schoolar("Iva", 13);
            Schoolar schoolar2 = new Schoolar("Luda", 14);
            Schoolar copySchoolar = (Schoolar) schoolar.clone();

            System.out.println("Hash code: " + copySchoolar.hashCode());
            System.out.println(schoolar.toString() + " == " + copySchoolar.toString() + ": " + schoolar.equals(copySchoolar));
            System.out.println(schoolar.toString() + " == " + schoolar2.toString() + ": " + schoolar.equals(schoolar2));
            newLine(2);

            // Student
            title("Student");
            Student student = new Student("Kolya", 20);
            Student student2 = new Student("Marina", 19);
            Student copyStudent = (Student) student.clone();

            System.out.println("Hash code: " + copyStudent.hashCode());
            System.out.println(student.toString() + " == " + copyStudent.toString() + ": " + student.equals(copyStudent));
            System.out.println(student.toString() + " == " + student2.toString() + ": " + student.equals(student2));
            newLine(2);

            // Employee
            title("Employee");
            Employee employee = new Employee("Andrew", 23);
            Employee employee2 = new Employee("Katya", 25);
            Object copyEmployee = (Employee) employee.clone();

            System.out.println("Hash code: " + copyEmployee.hashCode());
            System.out.println(employee.toString() + " == " + copyEmployee.toString() + ": " + employee.equals(copyEmployee));
            System.out.println(employee.toString() + " == " + employee2.toString() + ": " + employee.equals(employee2));
            newLine(1);

            Student stud = new Student("Andriy", 18);
            Employee emp = new Employee("Andriy", 28);
            System.out.println(stud.toString() + " == " + emp.toString() + ": " + stud.equals(emp));
            newLine(2);


            // LeagueTeam
            title("LeagueTeam");
            LeagueTeam<Employee> team = new LeagueTeam<>("Dragon");
            team.addNewParticipant(employee);
            team.addNewParticipant(employee2);
            System.out.println("Main: " + team.toString());

            LeagueTeam<Employee> copyTeamByConstructor = new LeagueTeam<>(team);
            System.out.println("Copy by constructor: " + copyTeamByConstructor.toString());

            LeagueTeam<Employee> copyTeamByStaticMethod = LeagueTeam.getCopyInstance(team);
            System.out.println("Copy by static method: " + copyTeamByStaticMethod.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void title(String title) {
        System.out.println("----- " + title + " -----");
    }
    private static void newLine(int linesNumber) {
        for(int i = 0; i < linesNumber; i++) {
            System.out.println("");
        }
    }
}
