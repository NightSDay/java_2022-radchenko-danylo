package com.education.ztu;

import com.education.ztu.game.ParticipantAgeComparator;
import com.education.ztu.game.ParticipantNameThenAgeComparator;
import com.education.ztu.game.Student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main2 {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("Kolya", 10));
        list.add(new Student("Olya", 12));
        list.add(new Student("Olkash", 14));
        list.add(new Student("Ministr", 11));

        // Сортування учвснитків по імені
        title("Sorting by name");
        System.out.println("Before sorting \n" + list);
        Collections.sort(list);
        System.out.println("After sorting  \n" + list);
        newLine(1);

        // Порівняння учасників за віком
        title("Sorting by age");
        System.out.println("Before sorting \n" + list);
        Collections.sort(list, new ParticipantAgeComparator());
        System.out.println("After sorting  \n" + list);
        newLine(1);

        // Порівняння учасників по імені, потім за віком
        title("Sorting by name, then by age");
        System.out.println("Before sorting \n" + list);
        Collections.sort(list, new ParticipantNameThenAgeComparator());
        System.out.println("After sorting  \n" + list);
    }

    private static void title(String title) {
        System.out.println("----- " + title + " -----");
    }
    private static void newLine(int linesNumber) {
        for(int i = 0; i < linesNumber; i++) {
            System.out.println("");
        }
    }
}
