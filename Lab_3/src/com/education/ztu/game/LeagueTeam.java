package com.education.ztu.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LeagueTeam<T extends Participant> {
    private String name;
    private List<T> participants = new ArrayList<>();

    public LeagueTeam(String name) {
        this.name = name;
    }

    public LeagueTeam(String name, List<T> participants) {
        this.name = name;
        this.participants = participants;
    }

    // copy constructor
    public LeagueTeam(LeagueTeam<T> other) {
        this(other.name, other.participants);
    }

    // copy static method
    public static LeagueTeam getCopyInstance(LeagueTeam obj) {
        return new LeagueTeam(obj.name, obj.participants);
    }

    public void addNewParticipant(T participant) {
        participants.add(participant);
        if (participant instanceof Participant) {
            System.out.println("To the team " + name + " was added participant " + ((Participant) participant).getName());
        }
        else {
            System.out.println("To the team " + name + " was added participant " + participant);
        }
    }

    public void playWith(LeagueTeam<T> team) {
        String winnerName;
        Random random = new Random();
        int i = random.nextInt(2);
        if (i == 0) {
            winnerName = this.name;
        } else {
            winnerName = team.name;
        }
        System.out.println("The team " + winnerName + " is winner!");
    }

    public String getName() {
        return name;
    }

    public List<T> getParticipants() {
        return participants;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParticipants(List<T> participants) {
        this.participants = participants;
    }

    @Override
    public String toString() {
        return "LeagueTeam{" +
                "name='" + name + '\'' +
                ", participants=" + participants +
                '}';
    }
}
