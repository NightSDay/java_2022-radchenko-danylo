package com.education.ztu.game;

public class Game {
    public static void main(String[] args) {
//        Schoolar schoolar1 = new Schoolar("Ivan", 13);
//        Schoolar schoolar2 = new Schoolar("Mariya", 15);
//        Student student1 = new Student("Mykola", 20);
//        Student student2 = new Student("Viktoria", 21);
//        Employee employee1 = new Employee("Andriy", 28);
//        Employee employee2 = new Employee("Oksana", 25);
//        Team scollarTeam = new Team("Dragon");
//        scollarTeam.addNewParticipant(schoolar1);
//        scollarTeam.addNewParticipant(schoolar2);
//        scollarTeam.addNewParticipant(student1);
//        scollarTeam.addNewParticipant(employee1);
//
//        Team studentTeam = new Team("Vpered");
//        studentTeam.addNewParticipant(student1);
//        studentTeam.addNewParticipant(student2);
//        Team employeeTeam = new Team("Robotyagi");
//        employeeTeam.addNewParticipant(employee1);
//        employeeTeam.addNewParticipant(employee2);
//        Team anotherTeam = new Team("Fantaziya");
//        anotherTeam.addNewParticipant("hello");
//
//        Team scollarTeam2 = new Team("Rozumnyky");
//        Schoolar schoolar3 = new Schoolar("Sergey", 12);
//        Schoolar schoolar4 = new Schoolar("Olga", 14);
//        scollarTeam2.addNewParticipant(schoolar3);
//        scollarTeam2.addNewParticipant(schoolar4);
//        scollarTeam.playWith(scollarTeam2);
//        scollarTeam.playWith(employeeTeam);

        // Task 1
        title("Task 1");
        // Schoolar
        LeagueTeam<Schoolar> schoolarTeam1 = new LeagueTeam<>("'Cometas'");
        schoolarTeam1.addNewParticipant(new Schoolar("Ivan", 13));
        schoolarTeam1.addNewParticipant( new Schoolar("Mariya", 15));

        LeagueTeam<Schoolar> schoolarTeam2 = new LeagueTeam<>("'Texas'");
        schoolarTeam2.addNewParticipant(new Schoolar("Mark", 13));
        schoolarTeam2.addNewParticipant(new Schoolar("Anna", 14));

        schoolarTeam1.playWith(schoolarTeam2);
        schoolarTeam1.playWith(schoolarTeam2);
        schoolarTeam2.playWith(schoolarTeam1);
        newLine(1);

        // Student
        LeagueTeam<Student> studentTeam1 = new LeagueTeam<>("'Neverwinter'");
        studentTeam1.addNewParticipant(new Student("Mykola", 20));
        studentTeam1.addNewParticipant(new Student("Viktoria", 21));

        LeagueTeam<Student> studentTeam2 = new LeagueTeam<>("'Dragons'");
        studentTeam2.addNewParticipant(new Student("Alex", 22));
        studentTeam2.addNewParticipant(new Student("Marina", 20));

        studentTeam1.playWith(studentTeam2);
        studentTeam1.playWith(studentTeam2);
        studentTeam2.playWith(studentTeam1);
        newLine(1);

        // Employee
        LeagueTeam<Employee> employeeTeam1 = new LeagueTeam<>("'Neverwinter'");
        employeeTeam1.addNewParticipant(new Employee("Andriy", 28));
        employeeTeam1.addNewParticipant(new Employee("Oksana", 25));

        LeagueTeam<Employee> employeeTeam2 = new LeagueTeam<>("'Dragons'");
        employeeTeam2.addNewParticipant(new Employee("Anatoliy", 32));
        employeeTeam2.addNewParticipant(new Employee("Katerina", 27));

        employeeTeam1.playWith(employeeTeam2);
        employeeTeam1.playWith(employeeTeam2);
        employeeTeam2.playWith(employeeTeam1);
    }

    private static void title(String title) {
        System.out.println("----- " + title + " -----");
    }
    private static void newLine(int linesNumber) {
        for(int i = 0; i < linesNumber; i++) {
            System.out.println("");
        }
    }
}
