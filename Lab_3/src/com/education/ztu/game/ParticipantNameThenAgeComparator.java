package com.education.ztu.game;

import java.util.Comparator;

public class ParticipantNameThenAgeComparator implements Comparator<Participant> {

    private final ParticipantAgeComparator ageComparator = new ParticipantAgeComparator();

    @Override
    public int compare(Participant a, Participant b) {

        if (a.getName().compareTo(b.getName()) == 0) {
            return ageComparator.compare(a, b);
        } else {
            return a.getName().compareTo(b.getName());
        }
    }
}
