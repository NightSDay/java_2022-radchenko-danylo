package com.education.ztu.game;

import java.util.Comparator;

public class ParticipantAgeComparator implements Comparator<Participant> {

    @Override
    public int compare(Participant a, Participant b) {
        if (a.getAge() == b.getAge()) {
            return 0;
        } else if (a.getAge() < b.getAge()) {
            return -1;
        } else {
            return 1;
        }
    }
}
