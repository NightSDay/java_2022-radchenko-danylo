import java.util.*;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<List<String>> products = new ArrayList<>();
        products.add(Arrays.asList("Skincleaner", "LG", "2000", "4"));
        products.add(Arrays.asList("Mouse", "4Tech", "500", "30"));
        products.add(Arrays.asList("Laptop", "HP", "25000", "13"));
        products.add(Arrays.asList("Headphones", "JBL", "650", "27"));
        products.add(Arrays.asList("Wacom tablet", "Wacom", "420", "25"));

        // �������� �� ������ �� ������� � �������. (map)
        System.out.println("\nGet all brands and output to console.");
        Set<String> brands = products.stream().map(p -> p.get(1)).collect(Collectors.toSet());
        brands.forEach(System.out::println); // lambda expression


        // �������� 2 ������ ���� ���� ����� ������. (filter, limit)
        System.out.println("\nGet 2 products whose price is less than a thousand.");
        List<String> twoProductsLess1000 = new ArrayList<>();
        products.stream().filter(product -> Integer.parseInt(product.get(2)) < 1000).limit(1).forEach(p -> twoProductsLess1000.add(p.get(2)));
        twoProductsLess1000.forEach(System.out::println);


        // �������� ���� ��� ���� ������, �� � �� �����. (reduce)
        System.out.println("\nGet the sum of all types of goods in stock.");
        Integer result = products.stream().reduce(0, (sum, product) -> {
            return sum + Integer.parseInt(product.get(2)) * Integer.parseInt(product.get(3));
        }, Integer::sum);

        System.out.println(result);


        // ���������� ������ �� ������ (Collectors.groupingBy())
        System.out.println("\nGroup products by brand.");
        Map<Object, List<List<String>>> groupedByBrand = products.stream().collect(Collectors.groupingBy(p -> p.get(1)));
        for (Map.Entry<Object, List<List<String>>> brand : groupedByBrand.entrySet()) {
            System.out.println(brand.getKey());
            for (List<String> name : brand.getValue()) {
                System.out.println("- " + name.get(0));
            }
        }


        // ³���������� ������ �� ���������� ���� �� ��������� ����� (sorted, Collectors)
        System.out.println("\nSort products by increasing price and return an array.");
        products.stream().sorted(new ProductComparator())
                .forEach(p -> System.out.printf("%s (%s) - %s \n",
                        p.get(1), p.get(0), p.get(2)));
    }
}

