public class Task2 {
    public static void main(String[] args) {
        Printable operation = (str) -> System.out.println(str);
        operation.print("Danylo Radchenko");
    }
}

interface Printable {
    void print(String str);
}


