import java.util.Arrays;
import java.util.Optional;

public class Task6 {
    public static void main(String[] args) {
        Integer[] arr = {1, 2, 4, 6, 7, 2, 7};
        Integer[] arrEmpty = {};

        Optional<Integer> max = Arrays.stream(arr).max(Integer::compare);
        max.ifPresentOrElse(
                res -> System.out.println(res),
                () -> System.out.println("There are no numbers")
        );

        max = Arrays.stream(arrEmpty).max(Integer::compare);
        max.ifPresentOrElse(
                res -> System.out.println(res),
                () -> System.out.println("There are no numbers")
        );
    }
}
