import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Task3 {
    public static void main(String[] args) {

        // a
        Predicate<String> isNumber = (str)->{
            if (str == null) {
                return false;
            }
            try {
                double d = Double.parseDouble(str);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        };

        System.out.println(isNumber.test("123-"));

        Predicate<String> contains4 = t -> t.contains("4");
        Predicate<String> contains5 = t -> t.contains("5");
        System.out.println(contains4.and(contains5).test("5234"));


        // b
        Consumer<String> lessonSchedule = (timeStart) -> {
            System.out.println("Class starts at " + timeStart);
            LocalTime t = LocalTime.parse(timeStart) ;
            LocalTime timeEnd = t.plusMinutes(60);
            System.out.println("Class ends at " + timeEnd);
        };

        lessonSchedule.accept("08:30");


        // c
        String str = "I am working right now";
        Supplier<String> toUpperCase = () -> str.toUpperCase();
        System.out.println(toUpperCase.get());


        // d
        Function<String, Integer> getSum = (numbers) -> {
            String[] nums = numbers.split(" ");
            Integer sum = 0;
            for(String n : nums) {
                sum += Integer.parseInt(n);
            }
            return sum;
        };

        String numbers = "1 6 3 7 3 6 0 9";
        System.out.println(getSum.apply(numbers));
    }
}
