package com.education.ztu;


public class Car {
    protected  String brand;
    protected Engine engine;//у машини є атрибут бренд рядковий і змінна енжин
    //при створені без атрибутів автоматом встановлюєтсья unknown
    {
        this.brand = "Unknown";
    }

    public Car() {}

    public Car(String brand) {//конструктор з параметрами,передаєтсяь бренд і встановлюєтсяь енжин
        setBrand(brand);
        this.engine = new Engine();
    }

    public boolean engineIsWorks() {
        return this.engine.isEngineWorks();
    }
    //метод на перевірку чи працює двигун і далі сетери гетери
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    class Engine {//в енжині є атрибути чи він працює і методи щоб запустити і зупинити
        private boolean engineWorks;

        public Engine() {
            this.engineWorks = false;
        }

        public void startEngine() {
            System.out.println("Start engine");
            setEngineWorks(true);
        }

        public void stopEngine() {
            System.out.println("Stop engine");
            setEngineWorks(false);
        }

        public boolean isEngineWorks() {
            System.out.println("Engine is working now");
            return engineWorks;
        }

        public void setEngineWorks(boolean engineWorks) {
            this.engineWorks = engineWorks;
        }
    }
}
