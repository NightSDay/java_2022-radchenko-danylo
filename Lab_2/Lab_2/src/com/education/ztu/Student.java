package com.education.ztu;

public class Student extends Person {
    public static int counter;
    protected String speciality;
    protected int course;
    protected String university;

    public Student(String speciality, int course, String university) {
        counter++;

        setSpeciality(speciality);
        setCourse(course);
        setUniversity(university);
    }

    public Student(String firstname, String lastname, int age, Gender gender, Location location, String speciality, int course, String university) {
        super(firstname, lastname, age, gender, location);

        counter++;

        setSpeciality(speciality);
        setCourse(course);
        setUniversity(university);
    }
    @Override
    public void getOccupation() {
        System.out.println("I study at the university: " + this.university);
    }
    public static int showCounters() {
        return counter;
    }

    public String getFullInfo() {
        return "FullInfo: " + this.lastname + ", " + this.firstname + ", " + this.age + "y.o.," + this.gender + ", " + this.location +
                "Studies at " + this.university + " in the " + this.course + "speciality " + this.speciality;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }


}
