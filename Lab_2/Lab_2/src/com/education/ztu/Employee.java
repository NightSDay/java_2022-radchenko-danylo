package com.education.ztu;

public class Employee extends Person {
    public static int counter;
    protected Car car;
    protected String company;
    protected String position;

    public Employee(Car car, String company, String position) {
        counter++;

        setCompany(company);
        setPosition(position);
        setCar(car);
    }

    public Employee(String firstname, String lastname, int age, Gender gender, Location location, String company, Car car, String position) {
        super(firstname, lastname, age, gender, location);

        counter++;

        setCompany(company);
        setPosition(position);
        setCar(car);
    }

    @Override
    public void getOccupation() {
        System.out.println("My position: " + this.position);
    }

    public static int showCounters() {
        return counter;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }


}
