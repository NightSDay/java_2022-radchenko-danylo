package com.education.ztu;

public enum Location
{
    KYIV,
    ZHYTOMYR,
    HMELNITSKIY,
    CHERNIVTSI;
}
