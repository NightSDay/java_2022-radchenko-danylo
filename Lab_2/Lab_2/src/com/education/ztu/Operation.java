package com.education.ztu;

public class Operation {
    public static int addition (int ... num) {
        int res = 0;
        for (int i = 0; i < num.length; i++)
        {
            res += num[i];
        }
        return res;
    }

    public static int subtraction (int ... num) {
        int res = num[0];
        for (int i = 1; i < num.length; i++)
        {
            res -= num[i];
        }
        return res;
    }

    public static int multiplication (int ... num) {
        int res = 1;
        for (int i = 0; i < num.length; i++)
        {
            res *= num[i];
        }
        return res;
    }

    public static int division (int ... num) {
        int res = num[0];
        for (int i = 1; i < num.length; i++)
        {
            res /= num[i];
        }
        return res;
    }

    public static int averange (int ... num) {
        int res = 0;
        for (int i = 0; i < num.length; i++)
        {
            res += num[i];
        }
        return res/num.length;
    }

    public static int maximum (int ... num) {
        int res = Math.max(num[0], num[1]);
        for (int i = 2; i < num.length; i++)
        {
            res = Math.max(res, num[i]);
        }
        return res;
    }

    public static int minimum (int ... num) {
        int res = Math.min(num[0], num[1]);
        for (int i = 2; i < num.length; i++)
        {
            res = Math.min(res, num[i]);
        }
        return res;
    }
}
