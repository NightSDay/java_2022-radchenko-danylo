package com.education.ztu;

public interface Human
{
    String fullInfo = "Human";
    void sayAge();
    void sayGender();
    void sayLocation();
    void sayFullName();
    default void whoIam()
    {
        System.out.println("I'm human");
    }
    default String getFullInfo()
    {
        return fullInfo;
    }
}
