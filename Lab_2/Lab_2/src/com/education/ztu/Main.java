package com.education.ztu;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("SUBARU");
        System.out.println(car.getBrand());
        car.engineIsWorks();
        car.setBrand("SHKODA");
        System.out.println(car.getBrand());
        car.getEngine().startEngine();
        car.engineIsWorks();
        car.getEngine().stopEngine();

        System.out.println("-------------------------------------");

        Teacher teacher = new Teacher("Danylo", "Radchenko", 19, Gender.MALE,
                Location.ZHYTOMYR, "Java", "Zhytomyr Polytechnic", car);

        teacher.whoIam();
        System.out.println(teacher.getFullInfo());
        teacher.getOccupation();
        teacher.sayAge();
        teacher.sayFullName();
        teacher.sayGender();
        teacher.sayLocation();
        System.out.println(teacher.getCar().engineIsWorks());
        teacher.getCar().getEngine().startEngine();
        System.out.println(teacher.getCar().engineIsWorks());

        System.out.println("-------------------------------------");

        Student student = new Student("IPZ", 3, "Zhytomyr Polytechnic");
        student.sayFullName();
        student.getOccupation();
        System.out.println(student instanceof Person);
    }
}