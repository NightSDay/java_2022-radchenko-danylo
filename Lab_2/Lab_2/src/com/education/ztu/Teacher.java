package com.education.ztu;

public class Teacher extends Person {

    public static int counter;
    protected Car car;
    protected String subject;
    protected String university;

    public Teacher(Car car, String subject, String university) {
        counter++;

        setSubject(subject);
        setUniversity(university);
        setCar(car);
    }

    public Teacher(String firstname, String lastname, int age, Gender gender, Location location,
                   String subject, String university, Car car) {
        super(firstname, lastname, age, gender, location);

        counter++;

        setSubject(subject);
        setUniversity(university);
        setCar(car);
    }

    @Override
    public void getOccupation() {
        System.out.println("I study at the university: " + this.university);
    }
    public static int showCounters() {
        return counter;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }


}
