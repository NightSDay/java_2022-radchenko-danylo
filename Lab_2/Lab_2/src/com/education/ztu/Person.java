package com.education.ztu;


public abstract class Person implements Human
{
    protected String firstname;
    protected String lastname;
    protected int age;
    protected Gender gender;
    protected Location location;

    {
        this.firstname = "Firstname";
        this.lastname = "Lastname";
        this.age = 0;
        this.gender = Gender.FEMALE;
        this.location = Location.ZHYTOMYR;
    }

    public Person() {}

    public Person(String firstname, String lastname, int age, Gender gender, Location location)
    {
        setFirstName(firstname);
        setLastName(lastname);
        setAge(age);
        setGender(gender);
        setLocation(location);
    }

    public abstract void getOccupation();

    @Override
    public void sayAge()
    {
        System.out.println("My age: " + this.age);
    }

    @Override
    public void sayGender()
    {
        System.out.println("My gender: " + this.gender);
    }

    @Override
    public void sayLocation()
    {
        System.out.println("My location: " + this.location);
    }

    @Override
    public void sayFullName()
    {
        System.out.println("My full name: " + this.lastname + " " + this.firstname);
    }

    @Override
    public String getFullInfo()
    {
        return "FullInfo: " + this.lastname + ", " + this.firstname + ", " + this.age + "y.o.," + this.gender + ", " + this.location;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstName(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastName(String lastname)
    {
        this.lastname = lastname;
    }

    public Gender getGender()
    {
        return gender;
    }

    public void setGender(Gender gender)
    {
        this.gender = gender;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }
}
