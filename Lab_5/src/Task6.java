/*
Завдання 6. Створити Map що містить пари (ключ, значення) - ім'я продукту та об'єкт продукту (клас Product).
Використовуємо клас HashMap,
Продемонструвати роботу з Map використовуючи методи
(put, get, containsKey, containsValue, clear, putIfAbsent, keySet, values, putAll, remove, size)
Викликати метод entrySet та продемонструвати роботу з набором значень, що він поверне (getKey, getValue, setValue)
 */


import java.util.HashMap;
import java.util.Map;

public class Task6 {
    public static void main(String[] args) {
        Product butter = new Product("Butter", 15.00f);
        Product chocoltka = new Product("Chocoltka", 20.00f);
        Product tea = new Product("Green Tea", 25.00f);
        Product coffee = new Product("Arabic Coffee", 30.00f);
        Product juice = new Product("Juice", 35.00f);

        Product yksys = new Product("yksys", 33.00f);

        Map<String, Product> map = new HashMap<String, Product>();

        // put, get
        map.put(butter.getName(), butter);
        map.put(chocoltka.getName(), chocoltka);
        map.put(tea.getName(), tea);
        map.put(coffee.getName(), coffee);
        map.put(juice.getName(), juice);
        System.out.println(map);
        System.out.println("get: " + map.get("butter"));

        // containsKey, containsValue
        System.out.println("containsKey [\"butter\"]: " + map.containsKey("Water"));
        System.out.println("containsKey [\"Soda\"]: " + map.containsKey("Soda"));
        System.out.println("containsValue [water]: " + map.containsValue(butter));
        System.out.println("containsValue [soda]: " + map.containsValue(yksys));

        // clear
        map.clear();
        System.out.println("clear: " + map);

        // putIfAbsent
        map.putIfAbsent(butter.getName(), butter);
        map.putIfAbsent(chocoltka.getName(), chocoltka);
        System.out.println(map);

        // keySet, values
        System.out.println("keySet: " + map.keySet());
        System.out.println("values: " + map.values());

        // remove, size
        map.remove("butter");
        System.out.println("remove: " + map);
        System.out.println("size: " + map.size());
    }
}
