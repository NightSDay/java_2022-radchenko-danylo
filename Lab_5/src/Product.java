/*
* Завдання 2. Створити клас Product та задати йому поля та методи на власний вибір.
* */

public class Product {//створив об'єкт продукт
    private String name = "unknown";//додав два поля name і прайс
    private float price = 0.00f;

    public Product(String name, float price) {//зробив для нього конструктора з 2 параметрами
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {//гетери сетери
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {//перевизначив метод ту стірнг
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
