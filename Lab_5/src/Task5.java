/*
Завдання 5. Створити множину, що містить об'єкти класу Product:
Використовуємо клас TreeSet.
Продемонструвати роботу з множиною використовуючи методи
(add, first, last, headSet, subSet, tailSet, ceiling, floor, higher, lower, pollFirst, pollLast, descendingSet)
 */

import java.util.Comparator;
import java.util.TreeSet;

public class Task5 {
    public static void main(String[] args) {
        Product butter = new Product("Butter", 15.00f);
        Product chocoltka = new Product("Chocoltka", 20.00f);
        Product tea = new Product("Green Tea", 25.00f);
        Product coffee = new Product("Arabic Coffee", 30.00f);
        Product juice = new Product("Juice", 35.00f);

        Product yksys = new Product("yksys", 33.00f);


        TreeSet<Product> treeSet = new TreeSet<Product>(Comparator.comparing(Product::getName));

        // add  отримування елементів
        treeSet.add(butter);
        treeSet.add(chocoltka);
        treeSet.add(tea);
        treeSet.add(coffee);
        treeSet.add(juice);
        System.out.println(treeSet);

        // first, last
        System.out.println("first: " + treeSet.first());
        System.out.println("last: " + treeSet.last());

        // headSet
        System.out.println("headset: " + treeSet.headSet(juice));

        // subSet
        System.out.println("subSet: " + treeSet.subSet(juice, tea));

        // ceiling, floor
        System.out.println("ceiling: " + treeSet.ceiling(yksys));
        System.out.println("floor: " + treeSet.floor(yksys));

        // lower, higher
        System.out.println("lower: " + treeSet.lower(tea));
        System.out.println("higher: " + treeSet.higher(tea));

        // pollFirst, pollLast
        System.out.println("pollFirst: " + treeSet.pollFirst());
        System.out.println("pollLast: " + treeSet.pollLast());

        // descendingSet
        System.out.println("descendingSet: " + treeSet.descendingSet());
    }
}
