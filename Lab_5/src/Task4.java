/*
* Завдання 4. Створити чергу, що містить об'єкти класу Product:
Використовуємо клас ArrayDeque - FIFO.
Продемонструвати роботу з чергою використовуючи методи
* (push, offerLast, getFirst, peekLast, pop, removeLast, pollLast та інші)
* */

import java.util.ArrayDeque;

public class Task4 { //працюємо з чергою
    public static void main(String[] args) {
        Product butter = new Product("Butter", 15.00f);
        Product chocoltka = new Product("Chocoltka", 20.00f);
        Product tea = new Product("Green Tea", 25.00f);
        Product coffee = new Product("Arabic Coffee", 30.00f);
        Product juice = new Product("Juice", 35.00f);

        ArrayDeque<Product> arrayDeque = new ArrayDeque<>();

        // push
        arrayDeque.push(butter);
        arrayDeque.push(chocoltka);
        arrayDeque.push(tea);
        System.out.println("Push 1st part: " + arrayDeque);
        arrayDeque.push(coffee);
        arrayDeque.push(juice);
        System.out.println("Push 2nd part: " + arrayDeque);

        // offerLast
        arrayDeque.offerLast(juice);
        System.out.println("OfferLast: " + arrayDeque);

        // getFirst
        System.out.println("Get first: " + arrayDeque.getFirst());

        // peek last
        System.out.println("Peek last: " + arrayDeque.peekLast());

        // pop
        System.out.println("Pop: " + arrayDeque.pop());
        System.out.println("After pop: " + arrayDeque);

        // removeLast
        System.out.println("Remove last: " + arrayDeque.removeLast());
        System.out.println("After remove last: " + arrayDeque);

        // pollLast
        System.out.println("Poll last: " + arrayDeque.pollLast());
        System.out.println("After poll last: " + arrayDeque);
    }
}
