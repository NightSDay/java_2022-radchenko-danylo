/*
Створити динамічний масив, що містить об'єкти класу Product:
Використовуємо клас ArrayList або LinkedList.
Продемонструвати роботу з масивом використовуючи різні методи
(add, addAll, get, indexOf, lastIndexOf, iterator, listIterator, remove,
set, sort, subList, clear, contains, isEmpty, retainAll, size, toArray)
* */

import java.util.*;

public class Task3 {
    public static void main(String[] args) {
        Product chocolatka = new Product("Chocolatka", 15.00f);
        Product butter = new Product("Butter", 20.00f);
        Product tea = new Product(" GreenTea", 25.00f);
        Product coffee = new Product("ArabicaCoffee", 30.00f);
        Product juice = new Product("Juice", 35.00f);

        // add
        ArrayList<Product> arrayList = new ArrayList<>();
        arrayList.add(chocolatka);
        arrayList.add(butter);
        arrayList.add(tea);

        // addAll
        ArrayList<Product> productsList = new ArrayList<>();
        productsList.add(coffee);
        productsList.add(juice);
        productsList.add(tea);

        arrayList.addAll(productsList);

        // get, indexOf, lastIndexOf
        System.out.println("Gotten product with index [0]: " + arrayList.get(0));
        System.out.println("Index of object [ArabicaCoffee]: " + arrayList.indexOf(coffee));
        System.out.println("Last index of [GreenTea]: " + arrayList.lastIndexOf(tea));

        // iterator, listIterator
        Iterator<Product> iterator = arrayList.iterator();
        ListIterator<Product> listIterator = arrayList.listIterator();

        // remove
        arrayList.remove(butter);
        System.out.println("Remove [Butter]: " + arrayList);

        // set
        arrayList.set(4, butter);
        System.out.println("Set [Butter] in position [4]: " + arrayList);

        // sort
        arrayList.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        System.out.println("Sorted list by name: " + arrayList);

        // subList, contains, isEmpty, size
        System.out.println("SubList [1,3]: " + arrayList.subList(1, 3));
        System.out.println("Contains [juice]: " + arrayList.contains(juice));
        System.out.println("isEmpty: " + arrayList.isEmpty());
        System.out.println("size: " + arrayList.size());

        // retainAll
        arrayList.retainAll(productsList);
        System.out.println("retainAll: " + arrayList);

        // toArray
        Object[] arr = arrayList.toArray();

        // clear
        arrayList.clear();
        System.out.println("clear: " + arrayList);
    }
}
