/*
Завдання 7. Продемонструвати роботу з класом Collections:
Для роботи використати масив створений через Arrays.asList
Метод Collections.sort()
Метод Collections.binarySearch()
Методы Collections.reverse(), Collections.shuffle()
Метод Collections.fill()
Методы Collections.max(), Collections.min()
Метод Collections.copy()
Метод Collections.rotate()
Метод Collections.checkedCollection()
Метод Collections.frequency()
 */

import java.util.*;

public class Task7 {
    public static void main(String[] args) {
        Product butter = new Product("Butter", 15.00f);
        Product chocoltka = new Product("Chocoltka", 20.00f);
        Product tea = new Product("Green Tea", 25.00f);
        Product coffee = new Product("Arabic Coffee", 30.00f);
        Product juice = new Product("Juice", 35.00f);


        List<String> arrayList = Arrays.asList(butter.getName(), chocoltka.getName(), tea.getName(), coffee.getName(), juice.getName());
        System.out.println(arrayList);

        // sort
        Collections.sort(arrayList);
        System.out.println("sort: " + arrayList);

        // binarySearch
        System.out.println("binarySearch: " + Collections.binarySearch(arrayList, "butter"));

        // reverse, shuffle
        Collections.reverse(arrayList);
        System.out.println("reverse: " + arrayList);
        Collections.shuffle(arrayList);
        System.out.println("shuffle: " + arrayList);

        // fill
        Collections.fill(arrayList, "yksys");
        System.out.println("fill: " + arrayList);

        // max, min
        System.out.println("min: " + Collections.min(arrayList));
        System.out.println("max: " + Collections.max(arrayList));

        // copy
        List<String> newList = Arrays.asList("1", "2", "3", "4", "5");
        Collections.copy(newList, arrayList);
        System.out.println("copy: " + newList);

        // rotate зміщення
        Collections.rotate(arrayList, 2);
        System.out.println("rotate: " + arrayList);

        // checkedCollection
        System.out.println("checkedCollection [String.class]: " + Collections.checkedCollection(arrayList, String.class));

        // frequency
        System.out.println("frequency: " + Collections.frequency(arrayList, "yksys"));
    }
}
