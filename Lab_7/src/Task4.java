public class Task4 {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new ArithmeticProgressionThread(), "Thread1");
        Thread thread2 = new Thread(new ArithmeticProgressionThread(), "Thread2");
        Thread thread3 = new Thread(new ArithmeticProgressionThread(), "Thread3");
        thread1.start();
        thread2.start();
        thread3.start();
    }
}

class ArithmeticProgressionThread implements Runnable {

    static int result = 0;

    private static synchronized void increment() {
        result++;
        System.out.print(result + " ");
    }

    @Override
    public void run() {
        System.out.println("\n" + Thread.currentThread().getName() + " is running!");

        for (int i = 0; i <= 100; i++){
            increment();

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("\n" + Thread.currentThread().getName() + " completed!");
    }
}

