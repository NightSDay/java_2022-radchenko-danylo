public class Task3 {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnableThread(),"MyFlow");
        Thread thread2 = new Thread(new MyRunnableThread(),"MyFlow");
        Thread thread3 = new Thread(new MyRunnableThread(),"MyFlow");

        thread1.start();
        thread2.start();
        thread3.start();

        try{
            Thread.sleep(4000);
            thread1.interrupt();
            thread2.interrupt();
            thread3.interrupt();
        }
        catch(InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}

class MyRunnableThread implements Runnable {

    @Override
    public void run() {
        Thread currentThread = Thread.currentThread();
        System.out.println(currentThread.getName() + " running!");

        int counter = 0; // счетчик циклов
        while (!currentThread.isInterrupted() && counter <= 10000) {
            if (counter % 10 == 0)
                System.out.println(counter);
            counter++;
            try {
                Thread.sleep(1);
            }catch (InterruptedException e) {
                currentThread.interrupt();
            }
        }
        if (currentThread.isInterrupted()) {
            System.out.println("Розрахунок завершено!");
            return;
        }
        System.out.println(currentThread.getName() + " completed!");
    }
}
