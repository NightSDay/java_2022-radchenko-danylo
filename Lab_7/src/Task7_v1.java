import java.util.Random;

public class Task7_v1 {
    public static void main(String[] args) {
        long start, end;

        int[] arr = createRandomArr(1000000);

        start = System.currentTimeMillis();
        calculateArrElemsSum(arr);
        end = System.currentTimeMillis();

        System.out.println("Total time: " + (end - start) + " milliseconds");
    }

    private static int[] createRandomArr(int size) {
        int[] arr = new int[size];

        Random random = new Random();
        for (int i = 0; i < size; i++) {
            arr[i] = random.nextInt(100) - 50;
        }

        return arr;
    }

    private static void calculateArrElemsSum(int[] arr) {
        long sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        System.out.println("Total sum of array elements: " + sum);

    }
}
