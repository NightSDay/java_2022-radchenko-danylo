import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        MyString myString = new MyString();

        Thread reader = new Thread(new Reader(myString), "Reader");
        Thread printer = new Thread(new Printer(myString), "Printer");

        reader.start();
        printer.start();
    }
}

class MyString {
    private static String str = "";
    private static String action = "set";
    public synchronized void printString() {
        while (action.equals("print")) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        action = "print";
        System.out.println(str);
        notify();
    }

    public synchronized void setString(String newStr) {
        while (action.equals("set")) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        action = "set";
        str = newStr;
        notify();
    }
}

class Reader implements Runnable {
    private MyString myString;

    public Reader(MyString myString) {
        this.myString = myString;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String input = scanner.nextLine();
            if(input.equals("^C^C"))
                break;
            myString.setString(input);
        }
    }
}

class Printer implements Runnable {

    private MyString myString;

    public Printer(MyString myString) {
        this.myString = myString;
    }

    @Override
    public void run() {
        while (true) {
            myString.printString();
        }
    }
}
