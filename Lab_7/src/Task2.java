public class Task2 {
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        System.out.println("-- Flow State: " + myThread.getState());
        myThread.start();
        System.out.println("-- Flow State: " + myThread.getState());

        System.out.println("\nFlow Name: " + myThread.getName());
        System.out.println("Flow Priority: " + myThread.getPriority());
        System.out.println("Flow is Interrupted: " + myThread.isInterrupted());
        System.out.println("Flow is Alive: " + myThread.isAlive());
        System.out.println("Flow is Daemon: " + myThread.isDaemon());

        myThread.setName("my-Flow-1");
        System.out.println("\nSet Flow Name: " + myThread.getName());
        myThread.setPriority(1);
        System.out.println("Set Flow Priority: " + myThread.getPriority());

        try {
            Thread mainThread = Thread.currentThread();
            mainThread.join(5000);
            System.out.println("\nMain Flow Name: " + mainThread.getName());
            System.out.println("Main Flow Priority: " + mainThread.getPriority());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println("-- Flow State: " + myThread.getState());
    }
}

class MyThread extends Thread {

    @Override
    public void run() {
        System.out.println("\t" + this.getName() + " is running!");
        for (int i = 0; i < 100; i++)
            System.out.println("\tI love painting!!!");
        System.out.println("\t" + this.getName() + " completed!");
    }
}

