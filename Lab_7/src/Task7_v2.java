import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Task7_v2 {

    private final static int THREAD_AMOUNT = 5;

    public static void main(String[] args) {
        long start, end;

        int[] arr = createRandomArr(1000000);

        // однопоточність
        start = System.currentTimeMillis();
        int sum1 = calculateArrElemsSum(arr);
        end = System.currentTimeMillis();
        System.out.println("Single-threaded total time: " + (end - start) + " milliseconds. Sum: " + sum1);


        // багатопоточність
        ExecutorService service = Executors.newSingleThreadExecutor();
        MyThreadV2 myThreadV2 = new MyThreadV2(arr, THREAD_AMOUNT);

        start = System.currentTimeMillis();
        for(int i = 0; i < THREAD_AMOUNT; i++) {
            service.execute(myThreadV2);
        }
        service.shutdown();
        try {
            boolean res = service.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        end = System.currentTimeMillis();
        int sum2 = MyThreadV2.sum;// тут я отримую доступ до результату через статичну сум
        System.out.println("Multi-threaded total time: " + (end - start) + " milliseconds. Sum: " + MyThreadV2.sum);


        // порінвняння реузльтатів
        System.out.println("Sums are equal: " + (sum1 == sum2));
    }

    private static int[] createRandomArr(int size) {
        int[] arr = new int[size];

        Random random = new Random();
        for (int i = 0; i < size; i++) {
            arr[i] = random.nextInt(100) - 50;
        }

        return arr;
    }

    private static int calculateArrElemsSum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
}
class MyThreadV2 implements Runnable {

    static int sum = 0;
    private static int index = 0;
    private static int[] arr = new int[0];
    private static int nThreads = 0;

    public MyThreadV2(int[] arr, int nThreads) {//в конструктор передаєтсяь сам
        // масив і кількість птоків які будуть створені
        this.arr = arr;//далі я між ними розподіляю навантаження
        this.nThreads = nThreads;
    }

    private int[] countIndexes() {
        int startIndex = index;
        int endIndex = index + (arr.length / nThreads);
        index = endIndex;
        return new int[]{startIndex, endIndex};
    }

    private void addToSum(int n) {
        sum += n;
    }

    @Override
    public void run() {
        int[] indexes = countIndexes();
        for (int i = indexes[0]; i < indexes[1]; i++) {
            addToSum(arr[i]);//і вони кожен окремо додають
        }
    }
}
