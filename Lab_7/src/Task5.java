public class Task5 {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new ArithmeticProgressionThreadV2(), "Thread1");
        Thread thread2 = new Thread(new ArithmeticProgressionThreadV2(), "Thread2");
        Thread thread3 = new Thread(new ArithmeticProgressionThreadV2(), "Thread3");
        thread1.start();
        thread2.start();
        thread3.start();
    }
}

class ArithmeticProgressionThreadV2 implements Runnable {

    static int result = 0;

    @Override
    public void run() {
        System.out.println("\n" + Thread.currentThread().getName() + " is running!");

        for (int i = 0; i <= 100; i++){
            synchronized (this) {
                result++;
                System.out.print(result + " ");
            }

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("\n" + Thread.currentThread().getName() + " completed!");
    }
}

