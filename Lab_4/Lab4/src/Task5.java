import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;

public class Task5 {
    private static final String path = "resourse/";

    public static void main(String[] args) {
        Object[][] goods = new Object[][]{
                {"clothes.jeans", "category.female", 1500.78},
                {"clothes.skirt", "category.female", 1000.56},
                {"clothes.tie", "category.male", 500.78},
                {"clothes.t-shirt", "category.baby", 109.99},
                {"clothes.shorts", "category.female", 269.50},
                {"clothes.sweatshirt", "category.baby", 349.89},
                {"clothes.skirt", "category.baby", 1000.56}
        };

        Locale en_US = new Locale("en", "US");
        Locale ua_UA = new Locale("ua", "UA");
        Locale fr_FR = new Locale("fr", "FR");

        printReceipt(goods, en_US);
        printReceipt(goods, ua_UA);
        printReceipt(goods, fr_FR);
    }

    private static void printReceipt(Object[][] products, Locale locale) {
        ResourceBundle rb = ResourceBundle.getBundle(path + "receipt", locale);
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

        float sum = 0.00f;

        String border = "=".repeat(50);
        String preHeader = rb.getString("pre-header");
        String[] header = rb.getString("header").split(";");
        String footer = rb.getString("footer");

        System.out.println("\n");
        System.out.format("%-26s%26s%n", preHeader, dtf.format(localDateTime));
        System.out.format("%s%n", border);
        System.out.format("%-4s%-15s%-21s%-12s%n", "№", header[0], header[1], header[2]);
        System.out.format("%s%n", border);

        for(int item = 0; item < products.length; item++){
            String product = rb.getString(products[item][0].toString());
            String category = rb.getString(products[item][1].toString());

            System.out.format("%-4s%-15s%-21s%-12s%n", item + ".", product, category, nf.format(products[item][2]));
            sum += Float.parseFloat(products[item][2].toString());
        }

        System.out.format("%s%n", border);
        System.out.format("%-26s%26s %n", footer + ":", nf.format(sum));
        System.out.println("\n");
    }

}
