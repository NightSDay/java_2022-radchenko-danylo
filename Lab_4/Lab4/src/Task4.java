import java.time.LocalDateTime;
import java.lang.String;

import java.time.format.DateTimeFormatter;

public class Task4 {
    public static void main(String args[]) {
        Object[][] goods = new Object[][]{
                {"Толстовка", "Жіночий одяг", 1500.78},
                {"Плаття", "Жіночий одяг", 1000.56},
                {"Смокінг", "Чоловічий одяг", 500.78},
                {"капці", "Дитячий одяг", 219.99},
                {"Шуба", "Жіночий одяг", 555.50},
                {"підштанники", "Дитячий одяг", 349.89},
                {"кепка", "Дитячий одяг", 179.90}
        };

        printReceipt(goods);
    }

    private static void printReceipt(Object[][] goods) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        String border = "=".repeat(50);
        float sum = 0.00f;

        System.out.format("%-21s%21s%n", "Дата та час покупки", dtf.format(localDateTime));
        System.out.format("%s%n", border);
        System.out.format("%-4s%-11s%-18s%-9s%n", "№", "Товар", "Категорія", "Ціна");
        System.out.format("%s%n", border);

        for(int item = 0; item < goods.length; item++){
            System.out.format("%-4s%-11s%-18s%-9s%n", item + ".", goods[item][0], goods[item][1], goods[item][2] + " ₴");
            sum += Float.parseFloat(goods[item][2].toString());
        }

        System.out.format("%s%n", border);
        System.out.format("%-21s%21s%n", "Разом:", sum + " ₴");
    }
}
