import java.time.LocalDate;
import java.time.LocalDateTime;

public class Task6 {
    public static void main(String[] args) {
        LocalDateTime lDt = LocalDateTime.of(2022, 12, 19, 17, 42, 13);
        System.out.println(lDt);
        System.out.println("Day of week: " + lDt.getDayOfWeek());
        System.out.println("Day of month: " + lDt.getDayOfMonth());
        System.out.println("Day of year: " + lDt.getDayOfYear());
        System.out.println("Month: " + lDt.getMonth());
        System.out.println("Year: " + lDt.getYear());
        System.out.println("Hour: " + lDt.getHour());
        System.out.println("Minute: " + lDt.getMinute());
        System.out.println("Second: " + lDt.getSecond());

        LocalDateTime now = LocalDateTime.now();
        System.out.println("Lab date is before now: " + lDt.isBefore(now));
        System.out.println("Lab date is after now: " + lDt.isAfter(now));


        LocalDate lD = LocalDate.of(2022, 12, 19);
        System.out.println("Leap year: " + lD.isLeapYear());

        System.out.println(lD);
        lD = lD.plusDays(7);
        lD = lD.minusMonths(2);
        lD = lD.minusYears(12);
        System.out.println(lD);
    }
}
