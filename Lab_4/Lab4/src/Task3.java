public class Task3 {
    public static void main(String args[]) {
        StringBuilder str = new StringBuilder();

        int num1 = 2;
        int num2 = 18;

        str.append(num1).append("+").append(num2).append("=").append(num1+num2).append("\n");
        str.append(num1).append("-").append(num2).append("=").append(num1-num2).append("\n");
        str.append(num1).append("*").append(num2).append("=").append(num1*num2).append("\n");
        System.out.println(str);

        System.out.println("---------заміна '=' на 'рівняється'-------------");

        str = new StringBuilder(str);

        while(str.indexOf("=") != -1) {
            str.insert(str.indexOf("="), "рівняється");
            str.deleteCharAt(str.indexOf("="));
        }
        String str1 = str.toString();
        System.out.println(str1);

        System.out.println("---------заміна з replace-------------");

        str = new StringBuilder(str);
        while(str.indexOf("=") != -1) {
            int index = str.indexOf("=");
            str.replace(index, index + 1,"рівняється");
        }
        String str2 = str.toString();
        System.out.print(str2);

        System.out.println("---------заміна з reverse-------------");

        System.out.println(str.reverse());

        System.out.println("\nLength: " + str.length());
        System.out.println("Capacity: " + str.capacity());
    }
}
