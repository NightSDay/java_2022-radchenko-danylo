public class Task2 {
    public static void main(String args[]) {
        Tasks("I learn Java!!!");
    }

    private static void Tasks(String s) {
        System.out.println("Last char: " + s.charAt(s.length() - 1));
        System.out.println("Ends with '!!!': " + s.endsWith("!!!"));
        System.out.println("Starts with 'I learn': " + s.startsWith("I learn"));
        System.out.println("Contain 'Java': " + s.contains("Java"));
        System.out.println("Position 'Java': " + s.indexOf("Java"));
        System.out.println("Replace 'a' - 'o': " + s.replace("a", "o"));
        System.out.println("Upper case: " + s.toUpperCase());
        System.out.println("Lower case: " + s.toLowerCase());
        System.out.println("Cut 'Java': " + s.replace("Java", ""));
    }
}
