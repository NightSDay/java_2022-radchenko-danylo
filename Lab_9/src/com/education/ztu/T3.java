package com.education.ztu;

import java.util.Arrays;
import java.util.Collections;

public class T3 {
    public static void main(String[] args) {
        Product mouse = new Product(1, "BP", "Комп'ютерна техніка", 50);
        Product keyboard = new Product(2, "Keyboard", "Комп'ютерна техніка", 150);
        Product monitor = new Product(3, "Monitor", "Комп'ютерна техніка", 570);

        String str = "";

        Class aClass_1 = mouse.getClass();
        Class aClass_2 = null;
        try {
            aClass_2 = Class.forName("com.education.ztu.Product");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        Class aClass_3 = String.class;

        System.out.println(aClass_1);
        System.out.println(aClass_2);
        System.out.println(aClass_3);

        System.out.println(String.join("=", Collections.nCopies(50, str)));

        System.out.println("Declared Constructors: "
                + Arrays.toString(aClass_1.getDeclaredConstructors()));
        System.out.println("Constructors: "
                + Arrays.toString(aClass_1.getConstructors()));

        System.out.println("Declared Methods: "
                + Arrays.toString(aClass_1.getDeclaredMethods()));
        System.out.println("Methods: "
                + Arrays.toString(aClass_1.getMethods()));

        System.out.println("Declared Fields: "
                + Arrays.toString(aClass_1.getDeclaredFields()));
        System.out.println("Fields: "
                + Arrays.toString(aClass_1.getFields()));

        System.out.println(String.join("=", Collections.nCopies(50, str)));

        System.out.println(keyboard.getProductName());
        System.out.println(keyboard.getProductCategory());
        System.out.println(keyboard.getProductPrice());

        System.out.println(String.join("=", Collections.nCopies(50, str)));

        monitor.setProductName("Monitor Plus");
        System.out.println(monitor.getProductName());
    }
}

class Product{
    private int productId = -1;
    private String productName = " ";
    public String productCategory = " ";
    public int productPrice = 0;

    public Product(int productId, String productName, String productCategory, int productPrice) {
        this.productId = productId;
        this.productName = productName;
        this.productCategory = productCategory;
        this.productPrice = productPrice;
    }

    public Product(){

    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productCategory='" + productCategory + '\'' +
                ", productPrice=" + productPrice +
                '}';
    }
}


