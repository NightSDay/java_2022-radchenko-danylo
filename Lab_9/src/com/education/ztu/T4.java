package com.education.ztu;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

public class T4 {
    public static void main(String[] args) {
        TestExample testExample = new TestExample();
        Method methodExampleA = null;
        Method methodExampleB = null;
        Method methodExampleC = null;
        try {
            methodExampleA = testExample.getClass().getMethod("testA");
            methodExampleB = testExample.getClass().getMethod("testB");
            methodExampleC = testExample.getClass().getMethod("testC");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        Test testA = methodExampleA.getAnnotation(Test.class);
        Test testB = methodExampleB.getAnnotation(Test.class);
        Test testC = methodExampleC.getAnnotation(Test.class);
        System.out.println(testA.enabled());
        System.out.println(testB.enabled());
        System.out.println(testC.enabled());
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Test {
    public boolean enabled() default true;
}

class TestExample {

    @Test
    public void testA() {
        if (true)
            throw new RuntimeException("This test always failed");
    }

    @Test(enabled = false)
    public void testB() {
        if (false)
            throw new RuntimeException("This test always passed");
    }

    @Test(enabled = true)
    public void testC() {
        if (10 > 1) {
            System.out.println("Test");
        }
    }
}
