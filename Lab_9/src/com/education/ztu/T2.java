package com.education.ztu;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class T2 {
    public static void main(String[] args) {
        String text = "Радченко_Данило_Вадимович, 25.06.2003, Митець, radchenkodanilo@gmail.com, +380503133311\n" +
                "Григір_Ігор_Іванович, 13.05.2013, Програміст, igorich123@gmail.com, +380981236545\n" +
                "Пеьренко_Микола_Михайлович, 04.09.2003, Психолог, myhalych228@gmail.com, +380936952674\n" +
                "Шевцов_Олексій_Петрович, 13.01.1999, Маркетолог, petrovych@gmail.com, +380980788878\n" +
                "Шевченко_Мартин_Миколайович, 10.11.2009, Пітоніст, sava2003@gmail.com, +380673413547\n";
        System.out.println(text);

        System.out.println("---------------------------------");
        Pattern pattern_Phones = Pattern.compile("\\+380\\d{3}\\d{2}\\d{2}\\d{2}");
        Matcher matcher_Phones = pattern_Phones.matcher(text);
        while(matcher_Phones.find())
            System.out.println(text.substring(matcher_Phones.start(), matcher_Phones.end()));

        System.out.println("---------------------------------");
        Pattern pattern_Emails = Pattern.compile("([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}");
        Matcher matcher_Emails = pattern_Emails.matcher(text);
        while(matcher_Emails.find())
            System.out.println(text.substring(matcher_Emails.start(), matcher_Emails.end()));

        System.out.println("---------------------------------");
        ArrayList<String> oldDates = new ArrayList<>();
        Pattern pattern_Date = Pattern.compile("(\\d{1,2})\\.(\\d{1,2})(?:\\.(\\d{4}))?");
        Matcher matcher_Date = pattern_Date.matcher(text);
        while(matcher_Date.find())
            oldDates.add(text.substring(matcher_Date.start(), matcher_Date.end()));

        ArrayList<String> newDates = new ArrayList<>();
        newDates.add("14.12.2001");
        newDates.add("15.12.2002");
        newDates.add("17.12.2003");
        newDates.add("19.12.2004");
        newDates.add("21.12.2005");

        for (int i = 0; i < oldDates.size(); i++){
            for (int j = 0; j < newDates.size(); j++) {
                if(i == j){
                    text = text.replace(oldDates.get(i), newDates.get(j));
                }
            }
        }
        System.out.println(text);

        System.out.println("---------------------------------");
        ArrayList<String> old_Employee = new ArrayList<>();
        Pattern pattern_Employee = Pattern.compile(" [А-Яа-яІіЇїЙйєЄ-]+");
        Matcher matcher_Employee = pattern_Employee.matcher(text);
        while(matcher_Employee.find())
            old_Employee.add(text.substring(matcher_Employee.start(), matcher_Employee.end()));

        ArrayList<String> new_Employee = new ArrayList<>();
        new_Employee.add(" Главврач");
        new_Employee.add(" Хитропикий");

        text = text.replace(old_Employee.get(0), new_Employee.get(0));
        text = text.replace(old_Employee.get(2), new_Employee.get(1));
        text = text.replace(old_Employee.get(4), new_Employee.get(0));

        System.out.println(text);
    }
}